import description from './search_box_by_type.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-form-input',
};
